// console.log("Hello World");

// [section] Syntaxt, statement and comments
// Statements in programming are instruction that we tell the computer to perform.
// JS Statements usually end with with semicolon (;).
// Semicolon is not required in JS, but we will use it to help us train to locate where a statement ends.
// Syntax in programming, it is the set of rules that we describe how statement must be constructed.
// All lines blocks of code shouldbe written in specific manner to work.
 // This is due to how these code where initially programed to function in certain manner.

 	// comments are parts of the code that gets ignored by the language.
	// Comments are mean to describe that written code.


	/*towo types of comments
	1.single - ctrl forward slash
	2.multiline - ctrl shift forward slash

	*/

// [section] Variables
// Variables are used to contain data.
// Any information that is used by an application is stored in what we call the "memory"
// When we create variables, certain portions oa a device's memory is given a name that we call variables.

// This makes it easier for us to associate information stored in our devices to actual "name" about information.

// Declaring Variables
// Declaring Variables- it tells our devices that avariable name is created and is ready to store data.
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined",
// meaning the variable's value was not defined.

let myVariable = "Ada Lovelace";
// const myVariable = "Ada Lovelace";
// if we set vallue 

console.log(myVariable);

/*let myVariable;

console.log(myVariable);
-usefull for printing  values of variables or certain results of code into the google chrome browsers console.

constant use of this 
*/


/*
	Guides in writing variables:
		1. Use the let keyword folowed the variables name of your choose and use the assignment operator (=) to assign
		value.
		2. Variable names should start with lowercase with lowercase character, use camelcase for multiple words.
		3. For constant variables, use the 'const' keyword.

			Using let: we can change the value of the variable.
			using const: we cannot change the value of the variable.
		4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion.

*/

// Declare and initializing Varibles
// Initializing variable - the instance when a variable is given it's initial/ starting value

	// Syntax
		// let/const variableName =value;
let productName = "desktop computer";
console.log(productName)

let productPrice = 188999;
console.log(productPrice)

// In the context of certain appplication, some variables/information are constant and should not be changed.
// Example: the interest rate for loan, savings

const interest = 3.350;
console.log(interest)


// reassigning variable value
// reassigning a variable means changing it's initial or previous value into another value.
	// syntax
		// variableName = newValue;

productName = "laptop";
console.log(productName)
console.log(productName)

// let productName = "laptop";
// console.log(productName)
	// let variable cannot be re-declared within its scope.

// value of constants cannot be changed and will simply return anf error.
// interest = 4.489;
// console.log(interest)


// Reassigning Variable and Initilizing variables
let supplier;
// Initialization
supplier = "Zuitt Store";
// Reassignment
supplier = "Zuitt merch";

const name = "George Alfred Cabaccang";
// name="George Alfred Cabaccang"

// var vs let/const
	// some of ou may wonder why we used let and const keyword in
	// declaring a variable when we search online, we usually see var.
	// var - is also used in declaring variable. but var is an EcmaScript1
	// feature [ES1 (Javascript 1997)]
	// let/const was intorduced as a new feature in ES6 (2016)

	// what makes let/const different from var?

a = 5;
console.log(a)
var a;

// b = 6;
// console.log(b);
// let	b;

	/*let/const local/global scope
	  Scope essentially means where these variables are variables
	  for use.
	*/
	// let and const are block scoped.
	// A block is a chunk of code bounded by {}. A block in curly braces. anything /in the curly braces is a block.

	let outerVariable = "hello from the other side";
	// {
	// 	let innerVariable = "hello from the block";
	// 	console.log(innerVariable);
	// 	console.log(outerVariable);
	// }
	{
		let innerVariable = "hello from the second block";
		console.log(innerVariable);
		let outerVariable = "hello from the second block";
	}
	console.log(outerVariable);
	// console.log(innerVariable);


// Multiple variable declarations
// Multiple variables can be declared in one lines.


let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand)
console.log(productCode);
console.log(productBrand);


// Using a variable with a reserved keyword
// const let = "hello";
// console.log(let);


// [sections] Data types
// Strings- are series of character that create a word, a phrase, a sentnce or anything related to creating text.
// String in Javascript can be written using either single (') nad double ("") qout.
// In other programming language, only the double can be used for creating strings.

let country = "Philippines";
let province = 'Metro Manila';

// concatenate
// Multiple string values can be combined to create a single string using the "+" symbol.

let fullAddress = province + "," + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);


// The escape characters (\) in strings in combination with other characters can produce different effects.

// "\n" refers to creatingg a new line between text
let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early."

message = 'John\'s employees went home early.';
console.log(message);


// Numbers
// Integers/whole numbers
 let count = "26";
 let headcount = 26;
 console.log(count);
 console.log(headcount);

 // Decimal Numbers / Fractions
 let grade = 98.7;
 console.log(grade);

 // exponential Notation
 let planetDistance = 2e10;
 console.log(planetDistance);

 
 // string ang number concat, number will become string
console.log("John' grade last quarter is" + grade);
console.log(count+headcount);
// Ty coersioon


// Boolean
// Boolean values are normally used to store values relating to the state of certain things.
// This will be useful in further discussion about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log(isMarried);
console.log(inGoodConduct);

console.log("isMarried:" + isMarried);


// Arrays
// Arrays are a specialkind of data type that's used to store multiple values.

// Arrays can store diffent data types but is normally use to store similar data types.

	// Similar data types
		// Syntax
		// let/const arrayName = [elementA, elementB, ]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades[1]);
console.log(grades);

// different data types
// Storing different data types inside an array is not recommended because
// it will not make sense in the context of programming.

let details =["John", "Smith", 32, true];
console.log(details);

// objects
// objects are another specialking of data type that's used to mimic real worl objects/items
// they're used to create a complex data that contains pieces of inforation that are relevant to each other.

// Syntax:
	// let/const objectName = {
	// propertyA: value,
	// propertyB: value,

// }

let	person ={
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["09272773456", "8123 4567"],
	address:{
		houseNumber: '345',
		city: "Manila"
	}
};
console.log(person);
console.log(person.contact[1]);
console.log(person.address.city)

// typeof operator is used todetermine the tyepe of data or the value of a variable.

console.log(typeof person);
// Note: array is special type of object with methods and functions to manipulate it. We will discuss these methods in late sessions.
// (s22 Javascript-array manipulation)
console.log(typeof details);


/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */

const anime = ['one piece', 'one punch man', 'attck on titans'];
// anime=['kimetsu no yaiba'];
console.log(anime);
anime[0]='kimetsu no yaiba';
console.log(anime);
anime[5] = 'dxd';
console.log(anime);
console.log(anime[4]);

// Null
// It is used to intintionally expressed the absence of a value in a variable/initialization.
// Null simply mean that a data type was assigned to a variable but it does not hold any value/amount or its nullified
let	spouse = null;
console.log(spouse);
// undifined
// represents the state of a variable that has been declared but without value

let fullName;
console.log(fullName);

// undefined vs null
// The difference between undefined and nulls is that for undefined a variable was created but
// not provide a value